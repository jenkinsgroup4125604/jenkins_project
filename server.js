// const express = require('express')
// const cors = require('cors')

// const app = express()
// app.use(cors('*'))
// app.use(express.json())

// app.get('/', (request, response) => {
//     response.send('running inside a container --- Version 2')
// })

// app.listen(3000, '0.0.0.0', () => {
//     console.log('server started on port 3000')
// })


//////////////////////////

const express=require('express')
const cors=require('cors')
const mysql=require('mysql2')


const connection=mysql.createConnection({
    // host:'localhost',
    host:'172.17.0.2',
    port:3306,
    user:'root',
    password:'root',
    waitForConnections:true,
    // connectionLimit:10,
    database:'Students_Tb'
})


const app=express()
app.use(cors('*'))
app.use(express.json())
connection.connect()

////////////////////////////////////////////

app.get('/',(req,res)=>{
    connection.query('select * from Students_Tb',(error,result)=>{
        if(error!=null)
            res.send(error)
        else
            res.send(result)    
    })
    
})

app.post('/',(req,res)=>{
    const query=`insert into Students_Tb values(default,'${req.body.s_name}','${req.body.password}','${req.body.course}','${req.body.passing_year}','${req.body.prn_no}','${req.body.dob}')`
    connection.query(query,(error,result)=>{
        if(error!=null)
            res.send(error)
        else
            res.send(result)  
    })
})
app.put('/:no',(req,res)=>{
    const query=`update Students_Tb set course='${req.body.course}', prn_no='${req.body.prn_no}' where student_id=${req.params.no}`;
    connection.query(query,(error,result)=>{
        if(error!=null)
        res.send(error)
        else
        res.send(result)  
    })
})

app.delete('/:no',(req,res)=>{
    const query=`delete from Students_Tb where student_id=${req.params.no}`
    connection.query(query,(error,result)=>{
        if(error!=null)
        res.send(error)
        else
        res.send(result)  
    })
})

////////////////////////////////////////
app.listen(3000,'0.0.0.0',()=>{
    console.log('server started at 3000   :)' );
})



